
(function(){
	var app = angular.module('techisland',[]);
	
	app.filter('numberFixedLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });
	
	app.controller('UserController',['$http','$scope',function($http,$scope){
		var userController = this;
		this.user = {pickupPoint:"SM North EDSA"};
		this.notFound = false;
		this.message = "Please enter your:";
		this.querying = false;
		this.getUser = function(){
			if(userController.user.id || userController.user.firstname ==''){
//				
			}else{
				userController.querying = true;
				var postRequest = $http.post('/getUsers',userController.user);
				postRequest.success(function(response,status){
					console.log(response);
					if(userController.user.id){
					
					}else if( response.id){
						userController.user = response;
						userController.notFound = false;
						userController.message  = "You are not yet registered.  Please enter your:";
						if(userController.user.hasJoinedRaffle){
							userController.message  = "You are already registered. You may edit your details below";
						}
					}else{
						userController.notFound = true;
						var lastname = userController.user.lastname;
						var firstname = userController.user.firstname;
						var mi = userController.user.mi;
						
						userController.user = {lastname:lastname,firstname:firstname,mi:mi};
						userController.message = "You are not yet registered.  Please enter your:";
					}
	//				
					userController.querying = false;
				});
				postRequest.error(function(response,status){
					userController.notFound = false;
					userController.message = "Please enter your:";
					userController.querying = false;
				});
			}
		};
		
		this.showDetails = function(){
			return !(userController.user.firstname == '' || userController.user.lastname == '');
		};
		
		this.saveUser = function(){
			console.log(userController.user);
			var postRequest = $http.post('/users',userController.user);
			postRequest.success(function(response,status){
				console.log(response);
				if(userController.user.hasJoinedRaffle){
					alert("Congratulations! Successful update of profile.");
				}else{
					alert("Congratulations! you are now registered.");
				}
				location.href='/';
			});
			postRequest.error(function(response,status){
				console.log(response);
				alert("Error with backend");
			});
		};
		

		this.saveWalkInUser = function(){
			console.log(userController.user);
			var postRequest = $http.post('/walkInUsers',userController.user);
			postRequest.success(function(response,status){
				console.log(response);
				if(userController.user.hasJoinedRaffle){
					alert("Congratulations! Successful update of profile.");
				}else{
					alert("Congratulations! you are now registered.");
				}
				location.href='/walkin';
			});
			postRequest.error(function(response,status){
				console.log(response);
				alert("Error with backend");
			});
		};
		
		
		this.cancelRegistration = function(){
			userController.user = {};
			userController.notFound = false;
		};
		
		
	}]);
	
	
})();