package controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class BondProvisioning {

	public static void main(String[] args) throws InvalidFormatException{
		File returnFile =null;
		try {
          File file = new File("D:\\HERTZ.xlsx");
          OPCPackage opcPackage = OPCPackage.open(file);
          XSSFWorkbook workbook = new XSSFWorkbook(opcPackage);            

          XSSFSheet myWorksheet = workbook.getSheetAt(0); 
          
          for(int row = 1; row<523; row ++){
        	  if(myWorksheet.getRow(row)!=null){
        		  XSSFRow rowExcel = myWorksheet.getRow(row);
        		  if(rowExcel.getCell(0)!=null)
        		  if(myWorksheet.getRow(row).getCell(0).getRawValue()!=null){
        			  if(!myWorksheet.getRow(row).getCell(0).getRawValue().isEmpty()){
        				 String plateNumber = rowExcel.getCell(1).getStringCellValue().replace(" ", "");
        				 String type = null;
        				 if(rowExcel.getCell(3)!= null){
        					 type = rowExcel.getCell(3).getStringCellValue();
        				 }
        				 String status ="Productive"; 
        				 String maker = null;
        				 if(rowExcel.getCell(5)!= null){
        					 maker = rowExcel.getCell(5).getStringCellValue();
        				 }
        				 String model = null;
        				 if(rowExcel.getCell(6)!= null){
        					 model = rowExcel.getCell(6).getStringCellValue();
        				 }
        				 
        				 int year = 0;
        				 if(rowExcel.getCell(7)!=null){
        					try{
        						 year = Integer.parseInt(rowExcel.getCell(7).getRawValue());
        				 	}catch(RuntimeException e){
        				 		
        				 	}
        				 }
        				 
        				 String location = null;
        				 if(rowExcel.getCell(9)!=null){
        					 location = rowExcel.getCell(9).getStringCellValue();
        				 }
        				 
        				 String color = null;
        				 if(rowExcel.getCell(11)!=null){
        					 color = rowExcel.getCell(11).getStringCellValue();
        				 }
        				 
        				 String engineSize;
        				 if(rowExcel.getCell(13)!=null){
        					 engineSize = rowExcel.getCell(13).getStringCellValue();
        				 }
        				 
        				 String fuelType;
        				 if(rowExcel.getCell(16)!=null){
        					 fuelType = rowExcel.getCell(16).getStringCellValue();
        				 }
        				 
        				  
        				 
        				 System.out.println(plateNumber+" "+color+" "+location+" "+year);
        				 
        				 
					  }
            	  }
        	  }
          }
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      } 
	
	}
	
	
	
	public static class MiniLedger{
		public String lastname = null;
		public String firstname = null;
		public Double bond;
		public Double shortBalance;
		public Double damageBalance;
	}
	
}
