package controllers;
import play.Application;
import play.GlobalSettings;
import play.Play;


public class Global extends GlobalSettings{

	public static String mbaasProfileApi;

	@Override
	public void onStart(Application arg0) {
		super.onStart(arg0);
		String mbaasHost = Play.application().configuration().getString("mbaas.profilemanagement.base.url");
		String tenantId = Play.application().configuration().getString("mbaas.profilemanagement.tenant.id");
		mbaasProfileApi = mbaasHost+"/tenants/"+tenantId+"/userProfiles";

	}
	
	
}
