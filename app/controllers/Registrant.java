package controllers;

import org.apache.commons.lang3.text.WordUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Registrant{
	@Override
	public String toString() {
		return "Registrant [id=" + id + ", lastname=" + lastname
				+ ", firstname=" + firstname + ", mi=" + mi + ", profession="
				+ profession + ", workAddress=" + workAddress
				+ ", contactNumber=" + contactNumber + ", pickupPoint="
				+ pickupPoint + ", hasJoinedRaffle=" + hasJoinedRaffle + "]";
	}

	public String id;
	public String lastname;
	public String firstname;
	public String mi;
	public String profession;
	public String workAddress;
	public String contactNumber;
	public String pickupPoint;
	public boolean hasJoinedRaffle;
	
	public boolean isLate = false;
	public boolean isWalkIn = false;
	
	public Registrant(){
		
	}
	
	public Registrant(JsonNode jsonNode){
		this.id = jsonNode.get("id").asText();

		if(jsonNode.has("lastname"))
		this.lastname = WordUtils.capitalizeFully(jsonNode.get("lastname").asText());

		if(jsonNode.has("firstname"))
		this.firstname = WordUtils.capitalizeFully(jsonNode.get("firstname").asText());

		if(jsonNode.has("mi"))
		this.mi = WordUtils.capitalizeFully(jsonNode.get("mi").asText());

		if(jsonNode.has("profession"))
		this.profession = WordUtils.capitalizeFully(jsonNode.get("profession").asText());

		if(jsonNode.has("workAddress"))
		this.workAddress = WordUtils.capitalizeFully(jsonNode.get("workAddress").asText());

		if(jsonNode.has("contactNumber"))
		this.contactNumber = WordUtils.capitalizeFully(jsonNode.get("contactNumber").asText());

		if(jsonNode.has("pickupPoint"))
		this.pickupPoint = jsonNode.get("pickupPoint").asText();


		if(jsonNode.has("isLate"))
		this.isLate = jsonNode.get("isLate").asBoolean();
		
		if(jsonNode.has("hasJoinedRaffle"))
		this.hasJoinedRaffle = jsonNode.get("hasJoinedRaffle").asBoolean();
		
		if(jsonNode.has("isWalkIn"))
		this.isWalkIn = jsonNode.get("isWalkIn").asBoolean();
	}
	
}