package controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import play.Logger;
import play.Play;
import play.libs.Json;
import play.libs.ws.WS;
import play.libs.ws.WSRequestHolder;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.admin;
import views.html.index;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.io.Files;

public class Application extends Controller {

	private static String templateFileLocation = Play.application()
			.configuration().getString("excel.template.location");
	private static String dataDirectory = Play.application().configuration()
			.getString("excel.data.dir");

	public static Result index() {
		return ok(index.render("Registration",false));
	}


	public static Result walkIn() {
		return ok(index.render("Registration",true));
	}

	
	public static Result getUser() {
		JsonNode jsonNodeRequest = request().body().asJson();
		Registrant registrationRequest = Json.fromJson(jsonNodeRequest,
				Registrant.class);
		if (registrationRequest.lastname != null
				&& registrationRequest.firstname != null
				) {
			if (registrationRequest.lastname.isEmpty()	|| registrationRequest.firstname.isEmpty()) {
				return badRequest();
			}

			WSRequestHolder holder = WS
					.url(Global.mbaasProfileApi)
					.setQueryParameter("lastname",
							"'" + registrationRequest.lastname + "'")
					.setQueryParameter("firstname",
							"'" + registrationRequest.firstname + "'")
					.setQueryParameter("ignoresCaseFields",
							"lastname,firstname,mi");
			if(registrationRequest.mi!=null){
				if(!registrationRequest.mi.trim().isEmpty()){
					holder.setQueryParameter("mi", "'" + registrationRequest.mi + "'");
				}
			}
			Logger.debug("logger:"+registrationRequest.toString());
			WSResponse response = holder.get().get(10000);
			Logger.debug(response.getBody().toString());
			JsonNode responseNode = response.asJson();
			ArrayNode arrayNode = (ArrayNode) responseNode;
			if (arrayNode.size() > 0) {
				return ok(Json.toJson(new Registrant(arrayNode.get(0))));
			} else {
				return ok(Json.toJson(new HashMap<String, Object>()));
			}
		} else {
			return badRequest();
		}
	}
	public static Result saveUser() {
		JsonNode jsonNodeRequest = request().body().asJson();
		Registrant registrant = Json.fromJson(jsonNodeRequest, Registrant.class);
		registrant.hasJoinedRaffle = true;
		
		if(registrant.id == null){
			registrant.isWalkIn = true;
		}
		
		
		WSRequestHolder holder = WS.url(Global.mbaasProfileApi);
		WSResponse response = holder.post(Json.toJson(registrant)).get(2000);
		Logger.debug(response.getBody().toString());
		if (response.getStatus() == CREATED) {
			return created();
		} else if (response.getStatus() == OK) {
			return ok();
		} else {
			return badRequest();
		}
	}
	

	public static Result saveWalkInUser() {
		JsonNode jsonNodeRequest = request().body().asJson();
		Registrant registrant = Json.fromJson(jsonNodeRequest, Registrant.class);
		registrant.hasJoinedRaffle = true;

		if(registrant.id == null){
			registrant.isWalkIn = true;
		}

		registrant.isLate = true;
		
		WSRequestHolder holder = WS.url(Global.mbaasProfileApi);
		WSResponse response = holder.post(Json.toJson(registrant)).get(2000);
		Logger.debug(response.getBody().toString());
		
		
		if (response.getStatus() == CREATED) {
			return created();
		} else if (response.getStatus() == OK) {
			return ok();
		} else {
			return badRequest();
		}
	}

	public static Result getUsers() {
		String pickup = request().getQueryString("pickup");
		WSRequestHolder holder = WS.url(Global.mbaasProfileApi).setQueryParameter("hasJoinedRaffle", "true");
		
		
		if (pickup != null) {
			if (!pickup.isEmpty()) {
				if(pickup.equals("late")){
					holder.setQueryParameter("isLate", "true");
				}else if(pickup.equals("walkin")){
					holder.setQueryParameter("isWalkIn", "true");
				}else{
					holder.setQueryParameter("pickupPoint", "'" + pickup + "'");
				}
				
			}
		}
		WSResponse response = holder.get().get(10000);
		ArrayNode arrayNode = (ArrayNode) response.asJson();
		Logger.debug(arrayNode.toString());
		List<Registrant> registrants = new ArrayList<Registrant>();
		for (int i = 0; i < arrayNode.size(); i++) {
			registrants.add(new Registrant(arrayNode.get(i)));
		}
		return ok(admin.render("Admin", registrants, pickup));
	}

	public synchronized static Result downloadExcel() {
		String pickup = request().getQueryString("pickup");
		WSRequestHolder holder = WS.url(Global.mbaasProfileApi)
				.setQueryParameter("hasJoinedRaffle", "true");

		if (pickup != null) {
			if (!pickup.isEmpty()) {
				if(pickup.equals("late")){
					holder.setQueryParameter("isLate", "true");
				}else if(pickup.equals("walkin")){
					holder.setQueryParameter("isWalkIn", "true");
				}else{
					holder.setQueryParameter("pickupPoint", "'" + pickup + "'");
				}
				
			}
		}
		WSResponse response = holder.get().get(10000);
		ArrayNode arrayNode = (ArrayNode) response.asJson();
		Logger.debug(arrayNode.toString());
		List<Registrant> registrants = new ArrayList<Registrant>();
		for (int i = 0; i < arrayNode.size(); i++) {
			registrants.add(new Registrant(arrayNode.get(i)));
		}
		try {
			if (pickup == null) {
				pickup = "All";
			}

			if (pickup.isEmpty()) {
				pickup = "All";
			}

			File newFile = new File(dataDirectory + "TechIsland registrants("+ pickup + ").xlsx");
			File file = new File(templateFileLocation);
			Files.copy(file, newFile);
			
			OPCPackage opcPackage = null;
			try {
				opcPackage = OPCPackage.open(file);
			} catch (InvalidFormatException e) {
				throw new RuntimeException(e);
			}
			XSSFWorkbook workbook = new XSSFWorkbook(opcPackage);

			XSSFSheet myWorksheet = workbook.getSheetAt(0);

			for (int i = 0; i < registrants.size(); i++) {
				Registrant registrant = registrants.get(i);
				XSSFRow row = myWorksheet.getRow(i + 1);
				if(row == null){
					row = myWorksheet.createRow(i+1);
					row.createCell(0);
					row.createCell(1);
					row.createCell(2);
					row.createCell(3);
					row.createCell(4);
					row.createCell(5);
					row.createCell(6);
					row.createCell(7);
					row.createCell(8);

				}
				if(registrant.lastname!= null){
					XSSFCell lastnameCell = row.getCell(0);
					lastnameCell.setCellValue(registrant.lastname);
				}

				if(registrant.firstname!= null){
					XSSFCell firstnameCell = row.getCell(1);
					firstnameCell.setCellValue(registrant.firstname);
				}

				if(registrant.mi!= null){
					XSSFCell miCell = row.getCell(2);
					miCell.setCellValue(registrant.mi);
				}

				if(registrant.profession!= null){
					XSSFCell professionCell = row.getCell(3);
					professionCell.setCellValue(registrant.profession);
				}

				if(registrant.workAddress!= null){
					XSSFCell workAddressCell = row.getCell(4);
					workAddressCell.setCellValue(registrant.workAddress);
				}

				if(registrant.contactNumber!= null){
					XSSFCell contactNumberCell = row.getCell(5);
					contactNumberCell.setCellValue(registrant.contactNumber);
				}

				if(registrant.pickupPoint!= null){
					XSSFCell pickupPointCell = row.getCell(6);
					pickupPointCell.setCellValue(registrant.pickupPoint);
				}

				XSSFCell walkInCell = row.getCell(7);
				if(registrant.isWalkIn){
					walkInCell.setCellValue("Yes");
				}else{
					walkInCell.setCellValue("No");
				}

				XSSFCell lateCell = row.getCell(8);
				if(registrant.isLate){
					lateCell.setCellValue("Yes");
				}else{
					lateCell.setCellValue("No");
				}
			}
//			 newFile = new File(dataDirectory + "TechIsland registrants("+ pickup + ").xlsx");
			FileOutputStream outputFile = new FileOutputStream(newFile);
			workbook.write(outputFile);
			outputFile.close();
			opcPackage.revert();
//			opcPackage.close();
			return ok(newFile);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}
	
	public Result provision() throws InvalidFormatException{
		File returnFile =null;
		try {
          File file = new File("D:\\HERTZ.xlsx");
          OPCPackage opcPackage = OPCPackage.open(file);
          XSSFWorkbook workbook = new XSSFWorkbook(opcPackage);            

          XSSFSheet myWorksheet = workbook.getSheetAt(0); 
          
          for(int row = 1; row<523; row ++){
        	  if(myWorksheet.getRow(row)!=null){
        		  XSSFRow rowExcel = myWorksheet.getRow(row);
        		  if(rowExcel.getCell(0)!=null)
        		  if(myWorksheet.getRow(row).getCell(0).getRawValue()!=null){
        			  if(!myWorksheet.getRow(row).getCell(0).getRawValue().isEmpty()){
        				 String plateNumber = rowExcel.getCell(1).getStringCellValue().replace(" ", "");
        				 String type = null;
        				 if(rowExcel.getCell(3)!= null){
        					 type = rowExcel.getCell(3).getStringCellValue();
        				 }
        				 String status ="Productive"; 
        				 String maker = null;
        				 if(rowExcel.getCell(5)!= null){
        					 maker = rowExcel.getCell(5).getStringCellValue();
        				 }
        				 String model = null;
        				 if(rowExcel.getCell(6)!= null){
        					 model = rowExcel.getCell(6).getStringCellValue();
        				 }
        				 
        				 int year = 0;
        				 if(rowExcel.getCell(7)!=null){
        					try{
        						 year = Integer.parseInt(rowExcel.getCell(7).getRawValue());
        				 	}catch(RuntimeException e){
        				 		
        				 	}
        				 }
        				 
        				 String location = null;
        				 if(rowExcel.getCell(9)!=null){
        					 location = rowExcel.getCell(9).getStringCellValue();
        				 }
        				 
        				 String color = null;
        				 if(rowExcel.getCell(11)!=null){
        					 color = rowExcel.getCell(11).getStringCellValue();
        				 }
        				 
        				 String engineSize = null;
        				 if(rowExcel.getCell(13)!=null){
        					 engineSize = rowExcel.getCell(13).getStringCellValue();
        				 }
        				 
        				 String fuelType = null;
        				 if(rowExcel.getCell(16)!=null){
        					 fuelType = rowExcel.getCell(16).getStringCellValue();
        				 }
        				 
        				 Map<String,Object> request = new HashMap<String,Object>();
        				 request.put("plateNumber",plateNumber);
        				 request.put("type",type);
        				 request.put("status",status);
        				 request.put("manufacturer",maker);
        				 request.put("model",model);
        				 request.put("year",year);
        				 request.put("location",location);
        				 request.put("color",color);
        				 request.put("engineSize",engineSize);
        				 request.put("fuelType",fuelType);
        				 
        				 WS.url("http://localhost:9000/api/vehicles").post(Json.toJson(request)).get(10000);
        				 
        				 System.out.println(plateNumber+" "+color+" "+location+" "+year);
        				 
        				 
					  }
            	  }
        	  }
          }
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      } 
		
		
		return TODO;
	}
	
	

}
